package pipeline4db

import (
	//	"fmt"
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"github.com/hu17889/go_spider/core/common/com_interfaces"
	"github.com/hu17889/go_spider/core/common/page_items"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"spider4movies/processer/movies"
	"time"
)

type PipelineMovie struct {
	mgoSession *mgo.Session
	dbconfig   map[string]string
}

func NewPipelineMovie(mgoSession *mgo.Session, dbconfig map[string]string) *PipelineMovie {
	return &PipelineMovie{mgoSession: mgoSession, dbconfig: dbconfig}
}

func (this *PipelineMovie) Process(items *page_items.PageItems, t com_interfaces.Task) {
	mgoSessionCopy := this.mgoSession.Copy()
	defer mgoSessionCopy.Close()

	println("-----Store To DB-------------------------------------------")
	println("Crawled Movie url :\t" + items.GetRequest().GetUrl() + "\n")
	println("Crawled result : ")

	var err error
	var movie movies.Movie
	collection := mgoSessionCopy.DB(this.dbconfig["database"]).C(this.dbconfig["collection"])
	//	collection := mgoSessionCopy.DB("movies").C("movies")
	for key, value := range items.GetAll() {
		println(key + "\t:\t" + value)

		if key == "jsonRet" {
			err = json.Unmarshal([]byte(value), &movie)
			if movie.Title == "" || movie.Url == "" {
				continue
			}
			//md5url、modified
			md5val := md5.Sum([]byte(movie.Url))
			movie.Md5Url = hex.EncodeToString(md5val[:])
			movie.Modified = time.Now().Format("2006-01-02 15:04:05")

			//err = collection.Insert(movie)
			_, err = collection.Upsert(bson.M{"md5url": movie.Md5Url}, movie)
			if err != nil {
				panic(err)
			}
		}

	}

	// TODO 存到DB并打印日志，写日志文件避免数据库出问题

}
