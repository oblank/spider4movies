// 配置需过滤不用的资源

package ignore
import "strings"

// config for ignore pics
func IsIgnorePic(domain string, url string) bool {
	urls := map[string]map[string]bool{
		"www.piaohua.com": {
			"/0701pic/allimg/120626/0223062024-7.jpg": true,
			"xxx": true,
		},
	}

	if tmp_urls, ok := urls[domain]; ok {
		if _, ok_b := tmp_urls[url]; ok_b {
			return true
		}
	}
	return false
}

// ignore url will not to be crawled
func IsIgnoreUrl(domain string, url string) bool {
	url = strings.Trim(url, " ")
	if url == "" || url == "/" {
		return true
	} else if url == "http://" + domain || url == domain {
		return true
	} else {
		return false
	}

	return false
}