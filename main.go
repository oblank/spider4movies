// spider project main.go
package main

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	//	"github.com/go-xorm/xorm"
	"github.com/hu17889/go_spider/core/scheduler"
	"github.com/hu17889/go_spider/core/spider"
	"gopkg.in/mgo.v2"
	"log"
	"spider4movies/pipeline4db"
	"spider4movies/processer"
	"time"
)

//var engine *xorm.Engine
//username:password@protocol(address)/dbname?param=value
//var mysql_db = "root:root@tcp(127.0.0.1:8889)/movies?charset=utf8"

const (
	MongoDBHosts = "127.0.0.1"
	AuthDatabase = "movies"
	Collection   = "movies"
	AuthUserName = ""
	AuthPassword = ""
	TestDatabase = "test"
)

func main() {
	var err error

	defer func() { // 必须要先声明defer，否则不能捕获到panic异常
		if err := recover(); err != nil {
			fmt.Println(err) // 这里的err其实就是panic传入的内容，55
		}
	}()

	//	engine, err = xorm.NewEngine("mysql", mysql_db)
	//	if err != nil {
	//		panic(err)
	//	}
	//	err = engine.Ping()
	//	if err != nil {
	//		panic(err)
	//	}
	//	defer engine.Close()

	// mgo
	mongoDBDialInfo := &mgo.DialInfo{
		Addrs:    []string{MongoDBHosts},
		Timeout:  60 * time.Second,
		Database: AuthDatabase,
		Username: AuthUserName,
		Password: AuthPassword,
	}

	dbconfig := map[string]string{
		"database":   AuthDatabase,
		"collection": Collection,
	}

	// Create a session which maintains a pool of socket connections
	mongoSession, err := mgo.DialWithInfo(mongoDBDialInfo)
	if err != nil {
		log.Fatalf("CreateSession: %s\n", err)
	}
	mongoSession.SetMode(mgo.Monotonic, true)

	// spider input:PageProcesser ;task name used in Pipeline for record;
	crawlIteration := true //是否迭代抓取URL
	Spider := spider.NewSpider(processer.NewPageProcesser4Movie(crawlIteration), "TaskName_Spider4Movie")
	println("\n--------------------------------spider.GetAll---------------------------------")
	urls := []string{
		//		"http://www.piaohua.com/html/dongzuo/2014/1222/29190.html",
		//				"http://www.piaohua.com/html/dongzuo/2015/0428/29710.html",
		"http://www.piaohua.com",
		//		"http://www.bttiantang.com/subject/26695.html",
		//		"http://www.bttiantang.com/subject/24335.html",
		"http://www.bttiantang.com",
		//"http://www.piaohua.com/html/aiqing/2015/0528/29822.html",
	}

	// Run 方式
	Spider.SetScheduler(scheduler.NewQueueScheduler(true)) // URL去重提高抓取效率
	Spider.SetSleepTime("rand", 2, 5)
	//TODO URL去重时要限定大小一定量的list
	Spider.
		AddUrls(urls, "html").                                             // 加入初始爬取链接，需要设置爬取结果类型，方便找到相应的解析器
		AddPipeline(pipeline4db.NewPipelineMovie(mongoSession, dbconfig)). // 引入PipelineConsole输入结果到标准输出，可同时使用多个
		SetThreadnum(3).                                                   // 设置爬取参数：并发个数
		Run()
}
