// 共用工具

package util

import (
	"strings"
)

//格式
var FormatKeyWords = []string{
	"高清", "标清",
	"hd1280", "bd1280", "1280", "1080", "720", "360",
	".mkv", ".mp4", ".rmvb", ".torrent", "ed2k", "thunder",
	"bt种子", "网盘",
}

//电影分类
var CatsKeyWords = []string{
	"喜剧", "搞笑", "纪录片", "犯罪", "家庭", "冒险",
	"动作", "爱情", "剧情", "科幻", "悬疑", "文艺",
	"恐怖", "灾难", "连续剧", "综艺",
	"惊悚", "动漫", "动画", "奇幻",
}

//截取子字符串
func Substr(s string, pos, length int) string {
	if s == "" {
		return ""
	}

	runes := []rune(s)
	l := pos + length
	if l > len(runes) {
		l = len(runes)
	}
	return string(runes[pos:l])
}

// get domain from url
func GetDomain(url string) string {
	url = strings.ToLower(url)
	if strings.HasPrefix(url, "http://") {
		url = url[7:]
	}
	if strings.HasPrefix(url, "https://") {
		url = url[8:]
	}
	index := strings.Index(url, "/")
	if index > 0 {
		return url[0:index]
	} else {
		return url
	}
}

//数组中是否包含
func ContainsInt(s []int, e int) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func ContainsString(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

// 获取视频的关键描述，高清、1080p、720p、BT、FTP、MKV、MP4...
func GetKeyWords4Video(str string) []string {
	if str == "" {
		return []string{}
	}

	str = strings.ToLower(str)
	var rets []string

	for _, keyWord := range FormatKeyWords {
		if keyWord == "ed2k" || keyWord == "thunder" {
			if strings.HasPrefix(str, keyWord) {
				rets = append(rets, keyWord)
				continue
			}
		}

		if strings.Index(str, keyWord) >= 0 {
			if keyWord == "hd1280" || keyWord == "bd1280" || keyWord == "bt种子" {
				keyWord = strings.ToUpper(keyWord)
			}
			rets = append(rets, keyWord)
		}
	}

	return rets
}

//获取分类
func GetMovieCatsByDescr(descr string) []string {
	if descr == "" {
		return []string{}
	}

	descr = Substr(descr, 0, 120) //从前150个字符中提供分类信息
	descr = strings.ToLower(descr)
	var rets []string
	for _, keyWord := range CatsKeyWords {
		if strings.Index(descr, keyWord) >= 0 {
			rets = append(rets, keyWord)
		}
	}

	return rets
}
