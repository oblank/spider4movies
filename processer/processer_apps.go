// 具体执行页面抓取工作的逻辑

package processer

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/hu17889/go_spider/core/common/page"
	"strings"
)

type PageProcesser4App struct {
	crawlIteration bool
}

func NewPageProcesser4App(crawlIteration bool) *PageProcesser4App {
	return &PageProcesser4App{crawlIteration: crawlIteration}
}

// Parse html dom here and record the parse result that we want to crawl.
// Package goquery (http://godoc.org/github.com/PuerkitoBio/goquery) is used to parse html.
func (this *PageProcesser4App) Process(p *page.Page) {
	if !p.IsSucc() {
		println(p.Errormsg())
		return
	}

	query := p.GetHtmlParser()

	// crawed other urls in this page
	var urls, pages []string
	query.Find("li a").Each(func(i int, s *goquery.Selection) {
		href, _ := s.Attr("href")
		if href != "" {
			//			fmt.Println("http://www.piaohua.com" + href)
			urls = append(urls, "http://www.piaohua.com"+href)
		}
	})
	p.AddTargetRequests(urls, "html")

	//pages
	query.Find(".page a").Each(func(i int, s *goquery.Selection) {
		href, _ := s.Attr("href")
		if href != "" {
			index_url := p.GetRequest().GetUrl()
			last_index_splash := strings.LastIndex(index_url, "/")
			index_url = index_url[0 : last_index_splash+1]
			index_url = strings.TrimRight(index_url, "index.html")
			//			fmt.Println(index_url + href)
			pages = append(pages, index_url+href)
		}

	})
	p.AddTargetRequests(pages, "html")
	// these urls will be saved and crawed by other coroutines.

	title := query.Find("h3").Text()
	if title == "" {
		return
	}

	name := query.Find("title").Text()
	name = strings.Trim(name, " \t\n")

	summary := query.Find("meta[name=Description]").Text()
	summary = strings.Trim(summary, " \t\n")

	showinfo := query.Find("#showinfo")
	descr := showinfo.Text()

	var imgs []string
	showinfo.Find("img").Each(func(i int, item *goquery.Selection) {
		src, _ := item.Attr("src")
		if src != "" {
			fmt.Printf("img:%i - %s\n", i, src)
			imgs = append(imgs, src)
		}
	})

	// the entity we want to save by Pipeline
	p.AddField("name", name)
	p.AddField("summary", summary)
	p.AddField("title", title)
	p.AddField("descr", descr)
	//	p.AddField("imgs", imgs)
}
