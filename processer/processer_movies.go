// 具体执行页面抓取工作的逻辑

package processer

import (
	"encoding/json"
	"fmt"
	"github.com/hu17889/go_spider/core/common/page"
	//	"os"
	"spider4movies/common/util"
	"spider4movies/processer/movies"
)

type PageProcesser4Movie struct {
	CrawlIteration bool
}

func NewPageProcesser4Movie(crawlIteration bool) *PageProcesser4Movie {
	return &PageProcesser4Movie{CrawlIteration: crawlIteration}
}

// Parse html dom here and record the parse result that we want to crawl.
// Package goquery (http://godoc.org/github.com/PuerkitoBio/goquery) is used to parse html.
func (this *PageProcesser4Movie) Process(p *page.Page) {
	if !p.IsSucc() {
		println(p.Errormsg())
		return
	}

	// get url domain
	domain := util.GetDomain(p.GetRequest().GetUrl())

	var item *movies.Movie
	var parser movies.Parser
	switch domain {
	case "www.piaohua.com", "piaohua.com":
		parser = movies.NewParser4PiaoHuaCom()
	case "www.bttiantang.com", "bttiantang.com":
		parser = movies.NewParser4BtTianTang()
	default:
		fmt.Println("Domain " + domain + " Has No parser")
		return
	}

	item = parser.ParsePage(domain, p)
	if parser.CheckIsList() { //list, not detail page
		fmt.Println("::page is list")
	} else {
		fmt.Println("::page is detail")
		// 将 item 进行json打包传输
		jsonRet, _ := json.Marshal(item)
		p.AddField("jsonRet", string(jsonRet[:]))
	}

	// add new target to request
	if this.CrawlIteration && len(item.GetLinksTo()) > 0 {
		p.AddTargetRequests(item.GetLinksTo(), "html")
	}

	//	if !parser.CheckIsList() {
	//		os.Exit(0)
	//	}
}
