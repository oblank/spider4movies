// 具体执行页面抓取工作的逻辑

package processer

import (
	//	"fmt"
	//	"github.com/PuerkitoBio/goquery"
	"github.com/hu17889/go_spider/core/common/page"
	//	"strings"
)

type MyPageProcesser interface {
	Process(p *page.Page)
}
