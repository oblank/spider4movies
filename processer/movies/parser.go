package movies

import "github.com/hu17889/go_spider/core/common/page"

type Parser interface {
	CheckIsList() bool
	ParsePage(domain string, p *page.Page) *Movie
}
