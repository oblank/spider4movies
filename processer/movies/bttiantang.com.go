// bttiantang.com process

package movies

import (
	"github.com/PuerkitoBio/goquery"
	"github.com/hu17889/go_spider/core/common/page"
	"github.com/kennygrant/sanitize"
	"spider4movies/common/util"
	"spider4movies/config"
	"strings"
)

type Parser4BtTianTang struct {
	IsList bool
	Item   *Movie
}

func NewParser4BtTianTang() *Parser4BtTianTang {
	return &Parser4BtTianTang{IsList: false, Item: &Movie{}}
}

func (this *Parser4BtTianTang) CheckIsList() bool {
	return this.IsList
}

func (this *Parser4BtTianTang) ParsePage(domain string, p *page.Page) *Movie {
	query := p.GetHtmlParser()

	this.Item.Url = p.GetRequest().GetUrl()

	// crawed other urls in this page
	//	var urls, pages []string
	query.Find(".rel a").Each(func(i int, s *goquery.Selection) {
		href, _ := s.Attr("href")
		if !ignore.IsIgnoreUrl(domain, href) {
			this.Item.LinksTo = append(this.Item.LinksTo, "http://"+domain+href)
		}
	})
	query.Find("#hotlst a").Each(func(i int, s *goquery.Selection) {
		href, _ := s.Attr("href")
		if !ignore.IsIgnoreUrl(domain, href) {
			this.Item.LinksTo = append(this.Item.LinksTo, "http://"+domain+href)
		}
	})
	query.Find(".title .tt a").Each(func(i int, s *goquery.Selection) {
		href, _ := s.Attr("href")
		if !ignore.IsIgnoreUrl(domain, href) {
			this.Item.LinksTo = append(this.Item.LinksTo, "http://"+domain+href)
		}
	})

	//pages
	query.Find(".Btitle a").Each(func(i int, s *goquery.Selection) {
		href, _ := s.Attr("href")
		if !ignore.IsIgnoreUrl(domain, href) {
			this.Item.LinksTo = append(this.Item.LinksTo, "http://"+domain+href)
		}
	})
	query.Find(".pagelist a").Each(func(i int, s *goquery.Selection) {
		href, _ := s.Attr("href")
		if !ignore.IsIgnoreUrl(domain, href) {
			this.Item.LinksTo = append(this.Item.LinksTo, "http://"+domain+href)
		}
	})
	//TODO: url 需要被处理，比如大小写，双反斜杠等
	// these urls will be saved and crawed by other coroutines.

	this.Item.PageTitle = query.Find("title").Text()

	title := query.Find(".title h2").Text()
	title = strings.Trim(title, "\r\n")
	if strings.Index(this.Item.Url, "/subject/") < 0 {
		this.IsList = true
		return this.Item // page is list page
	} else {
		this.IsList = false
	}

	this.Item.Title = title
	this.Item.TitleEn = strings.Trim(query.Find(".title h2 i").Text(), "/")

	summary := query.Find("meta[name=\"description\"]").Text()
	this.Item.Summary = strings.Trim(summary, " \t\n")

	showinfo := query.Find(".moviedteail")
	descr := showinfo.Find(".moviedteail_list")
	descr_html, _ := descr.Html()
	descr_html, _ = sanitize.HTMLAllowing(descr_html, []string{"br", "p", "li"}, []string{})

	descr_text := descr.Text()
	this.Item.Descr = strings.TrimSpace(descr_html)
	if this.Item.Descr != "" {
		this.Item.Cats = util.GetMovieCatsByDescr(descr_text)
	}

	showinfo.Find(".moviedteail_img").Each(func(i int, item *goquery.Selection) {
		src, _ := item.Attr("src")
		if src != "" {
			if ignore.IsIgnorePic(domain, src) == false {
				if strings.Index(src, "http://") < 0 {
					src = "http://" + domain + src
				}
				this.Item.Photos = append(this.Item.Photos, src)

				if i == 0 {
					this.Item.Posters = append(this.Item.Posters, src)
				}
			} else {
				//				fmt.Println(":: url is ignore:" + src)
			}

		}
	})
	//TODO: url 需要被处理，比如大小写，双反斜杠等

	// parse download urls
	this.Item.Download = make([]map[string]interface{}, 0)
	query.Find(".tinfo a").Each(func(i int, item *goquery.Selection) {
		href, _ := item.Attr("href")
		html, _ := item.Attr("title")
		//百度网盘可能附带密码
		if util.GetDomain(href) == "pan.baidu.com" {
			html = item.ParentFiltered("td").Text()
			html = strings.TrimSpace(strings.Trim(html, "\r\n"))
		}
		if !ignore.IsIgnoreUrl(domain, href) {
			if strings.HasPrefix(href, "http://") {
				return
			}
			href = "http://" + domain + href
			info := make(map[string]interface{})
			info["tags"] = util.GetKeyWords4Video(html)
			info["url"] = href
			info["title"] = html
			this.Item.Download = append(this.Item.Download, info)
		}
	})

	return this.Item
}
