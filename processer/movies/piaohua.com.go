// piaohua.com process

package movies

import (
	"github.com/PuerkitoBio/goquery"
	"github.com/hu17889/go_spider/core/common/page"
	"github.com/kennygrant/sanitize"
	"spider4movies/common/util"
	"spider4movies/config"
	"strings"
)

type Parser4PiaoHuaCom struct {
	IsList bool
	Item   *Movie
}

func NewParser4PiaoHuaCom() *Parser4PiaoHuaCom {
	return &Parser4PiaoHuaCom{IsList: false, Item: &Movie{}}
}

func (this *Parser4PiaoHuaCom) CheckIsList() bool {
	return this.IsList
}

func (this *Parser4PiaoHuaCom) ParsePage(domain string, p *page.Page) *Movie {
	query := p.GetHtmlParser()

	this.Item.Url = p.GetRequest().GetUrl()

	// crawed other urls in this page
	//	var urls, pages []string
	query.Find("li a").Each(func(i int, s *goquery.Selection) {
		href, _ := s.Attr("href")
		if !ignore.IsIgnoreUrl(domain, href) {
			this.Item.LinksTo = append(this.Item.LinksTo, "http://www.piaohua.com"+href)
		}
	})

	//pages
	query.Find(".page a").Each(func(i int, s *goquery.Selection) {
		href, _ := s.Attr("href")
		if !ignore.IsIgnoreUrl(domain, href) {
			index_url := p.GetRequest().GetUrl()
			last_index_splash := strings.LastIndex(index_url, "/")
			index_url = index_url[0 : last_index_splash+1]
			index_url = strings.TrimRight(index_url, "index.html")
			this.Item.LinksTo = append(this.Item.LinksTo, index_url+href)
		}
	})
	//TODO: url 需要被处理，比如大小写，双反斜杠等
	// these urls will be saved and crawed by other coroutines.

	this.Item.PageTitle = query.Find("title").Text()

	title := query.Find("h3").Text()
	title = strings.Trim(title, "\r\n")
	if title == "" {
		this.IsList = true
		return this.Item // page is list page
	} else {
		this.IsList = false
	}

	this.Item.Title = title
	//	fmt.Println(title)

	summary := query.Find("meta[name=Description]").Text()
	this.Item.Summary = strings.Trim(summary, " \t\n")

	showinfo := query.Find("#showinfo")
	descr_html, _ := showinfo.Html()
	descr_html, _ = sanitize.HTMLAllowing(descr_html, []string{"br", "p", "img"}, []string{"src"})
	descr_text := showinfo.Text()
	this.Item.Descr = strings.TrimSpace(descr_html)
	if this.Item.Descr != "" {
		this.Item.Cats = util.GetMovieCatsByDescr(descr_text)
	}

	showinfo.Find("img").Each(func(i int, item *goquery.Selection) {
		src, _ := item.Attr("src")
		if src != "" {
			if ignore.IsIgnorePic(domain, src) == false {
				if strings.Index(src, "http://") < 0 {
					src = "http://" + domain + src
				}
				this.Item.Photos = append(this.Item.Photos, src)

				if i == 0 {
					this.Item.Posters = append(this.Item.Posters, src)
				}
			} else {
				//				fmt.Println(":: url is ignore:" + src)
			}
		}
	})
	//TODO: url 需要被处理，比如大小写，双反斜杠等

	// parse download urls
	this.Item.Download = make([]map[string]interface{}, 0)
	showinfo.Find("table a").Each(func(i int, item *goquery.Selection) {
		href, _ := item.Attr("href")
		html, _ := item.Html()
		//百度网盘可能附带密码
		if util.GetDomain(href) == "pan.baidu.com" {
			html = item.ParentFiltered("td").Text()
			html = strings.TrimSpace(strings.Trim(html, "\r\n"))
		}
		if !ignore.IsIgnoreUrl(domain, href) {
			info := make(map[string]interface{})
			info["tags"] = util.GetKeyWords4Video(html)
			info["url"] = href
			info["title"] = html
			this.Item.Download = append(this.Item.Download, info)

		}
	})

	return this.Item
}
