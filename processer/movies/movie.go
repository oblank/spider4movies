// movie item

package movies

// status: normal, online, deleted, viponly
// visible: normal, viponly
// counts: {comment:113, }
// download: [720, 1080,mp4, mtv...]
// ext_info: {douban:{id:11134, start:7.4}, imdb:{id:34213, star:7.2}}
type Movie struct {
	Md5Url    string   `json:"md5url"`
	Url       string   `json:"url"`
	PageTitle string   `bson:"pageTitle,omitempty" json:"pageTitle,omitempty"`
	Title     string   `bson:"title" json:"title"`
	TitleEn   string   `bson:"titleEn,omitempty" json:"titleEn,omitempty"`
	Cats      []string `bson:"cats,omitempty" json:"cats,omitempty"`
	Status    string   `bson:"status,omitempty" json:"status,omitempty"`
	//	Visible string	`bson:"visible"`
	Summary  string                       `bson:"summary,omitempty" json:"summary,omitempty"`
	Descr    string                       `bson:"descr,omitempty" json:"descr,omitempty"`
	Posters  []string                     `bson:"posters,omitempty" json:"posters,omitempty"`
	Photos   []string                     `bson:"photos,omitempty" json:"photos,omitempty"`
	Download []map[string]interface{}     `bson:"download,omitempty" json:"download,omitempty"`
	Counts   map[string]int               `bson:"counts,omitempty" json:"counts,omitempty"`
	ExtInfo  map[string]map[string]string `bson:"extInfo,omitempty" json:"extInfo,omitempty"`
	LinksTo  []string                     `bson:"-" json:"-"`
	Modified string                       `json:"modified"`
}

func NewMovie() *Movie {
	return &Movie{}
}
func (this *Movie) GetUrl() string {
	return this.Url
}

func (this *Movie) GetPageTitle() string {
	return this.PageTitle
}

func (this *Movie) GetTitle() string {
	return this.Title
}

func (this *Movie) GetTitleEn() string {
	return this.TitleEn
}

func (this *Movie) GetSummary() string {
	return this.Summary
}

func (this *Movie) GetDescr() string {
	return this.Descr
}

func (this *Movie) GetPosters() []string {
	return this.Posters
}

func (this *Movie) GetPhotos() []string {
	return this.Photos
}

func (this *Movie) GetLinksTo() []string {
	return this.LinksTo
}

func (this *Movie) GetDownload() []map[string]interface{} {
	return this.Download
}
