-- phpMyAdmin SQL Dump
-- version 4.4.1.1
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: May 06, 2015 at 05:22 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `movies`
--
CREATE DATABASE IF NOT EXISTS `movies` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `movies`;

-- --------------------------------------------------------

--
-- Table structure for table `descrs`
--

DROP TABLE IF EXISTS `descrs`;
CREATE TABLE `descrs` (
  `_id` int(20) NOT NULL,
  `movie_id` int(20) NOT NULL,
  `title` varchar(300) NOT NULL,
  `descr` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `downloads`
--

DROP TABLE IF EXISTS `downloads`;
CREATE TABLE `downloads` (
  `_id` int(10) NOT NULL,
  `movie_id` varchar(20) NOT NULL,
  `title` tinytext NOT NULL,
  `count_dl` int(20) DEFAULT NULL,
  `tags` varchar(250) NOT NULL,
  `url` tinytext NOT NULL,
  `source` tinytext NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  `viewed` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `movies`
--

DROP TABLE IF EXISTS `movies`;
CREATE TABLE `movies` (
  `_id` int(20) NOT NULL,
  `title` varchar(300) NOT NULL,
  `titleEn` varchar(300) NOT NULL,
  `status` varchar(25) NOT NULL DEFAULT 'normal',
  `category` varchar(300) NOT NULL,
  `tags` varchar(300) DEFAULT NULL,
  `count_view` int(20) DEFAULT NULL,
  `count_comment` int(20) DEFAULT NULL,
  `count_dl` int(20) DEFAULT NULL,
  `douban` tinytext NOT NULL,
  `imdb` tinytext NOT NULL,
  `summary` tinytext NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `viewed` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

DROP TABLE IF EXISTS `photos`;
CREATE TABLE `photos` (
  `_id` int(20) NOT NULL,
  `movie_id` int(20) NOT NULL,
  `title` varchar(300) NOT NULL,
  `type` varchar(15) NOT NULL COMMENT 'poster, photo',
  `count_view` int(20) DEFAULT NULL,
  `count_comment` int(20) DEFAULT NULL,
  `url` varchar(300) NOT NULL,
  `descr` tinytext,
  `source` varchar(300) DEFAULT NULL,
  `source_site` varchar(150) DEFAULT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `descrs`
--
ALTER TABLE `descrs`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `movie_id` (`movie_id`);

--
-- Indexes for table `downloads`
--
ALTER TABLE `downloads`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `movie_id` (`movie_id`);

--
-- Indexes for table `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `status` (`status`),
  ADD KEY `category` (`category`(255));

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `type` (`type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `descrs`
--
ALTER TABLE `descrs`
  MODIFY `_id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `downloads`
--
ALTER TABLE `downloads`
  MODIFY `_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `movies`
--
ALTER TABLE `movies`
  MODIFY `_id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `_id` int(20) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
